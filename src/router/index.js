import { createRouter, createWebHistory } from 'vue-router';
import EventHandle from '../views/EventHandle/index.vue';
import FormInputBind from '../views/FormInputBind/index.vue';
import CompBasic from '../views/CompBasic/index.vue';

const routes = [
  {
    path: '/eventHandle',
    name: 'EventHandle',
    component: () => EventHandle
  },
  {
    path: '/formInputBind',
    name: 'FormInputBind',
    component: () => FormInputBind
  },
  {
    path: '/compBasic',
    name: 'CompBasic',
    component: () => CompBasic
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
