import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import { Quasar } from 'quasar';
import GlobalCompDemo1 from './views/CompBasic/components/demo1';
import quasarUserOptions from './quasar-user-options';

const app = createApp(App)
  .use(Quasar, quasarUserOptions)
  .use(router);

// 注册全局组件
app.component('global-comp-demo1', GlobalCompDemo1);
// 为什么这样注册不行
app.component('block-post-demo', {
  name: 'BlockPostDemo',
  props: ['title'],
  template: '<h4>{{ title }}</h4>'
});
// mount不反回应用本身，它返回的是根组件实例，放在最后
app.mount('#app');

// 按照文档上这样子写为什么没用
const app1 = createApp({});
app1.component('global-comp-demo1', GlobalCompDemo1);
app1.mount('#hhh');
